**Installation du projet API Platform avec pipeline CI**

> https://api-platform.com/docs/core/getting-started/
> 

 **1. Télécharger et installer la distribution API Plateform :** 

> https://github.com/api-platform/api-platform/releases/tag/v2.6.5
**Préférer le paquet en .tar.gz, l'archive zip pourrait causer des problèmes de permission.**

Créer un dossier dans C:\users\$user\Documents\ et y extraire le fichier "pax_global_header" et le dossier "api-platform-2.6.5".
 
**2. Télécharger et installer Docker Desktop**
> 
> La plateforme API inclut une définition Docker pour être en mesure
> d'avoir un environnement de développement clé en main.

Docker ne fonctionne pas en natif sous Windows,  il faut cocher l'option pour inclure WSL 2 dans l'installation.

**3. Démarrer les services liés à API Plateform avec Docker**

> `Docker-compose build --pull --no-cache`
> Cette commande va lire le fichier 'docker-compose.yml' et va configurer les services en suivant le système clé:valeur d'un fichier yaml. Ce processus est itératif, ne pas fermer l'invite de commande pendant son exécution.
> 
> `Docker-compose up -d`
> Cette commande utilise les versions précédemment installés et démarre un conteneur avec toute la couche applicative détaillée dans le fichier 'docker-compose.yml'. L'option '-d' démonise le conteneur ce qui lui permet de tourner en arrière-plan.

Vérifier l'accès à la plateforme via https://localhost.

**4. Création d'une CI gitlab**

   Créer un fichier yaml `.gitlab-ci.yml`
  
> Attention à bien respecter la syntaxe imposée par GitLab. Sinon le fichier ne sera pas détecté comme Pipeline CI et les itérations décrites à l'intérieur ne pourront pas être exécutées.

Décrire toute la pipeline CI à l'intérieur.

> En ce qui me concerne, j'ai basé ma pipeline sur deux étapes : test et deploy, qui sont respectivement liés aux environnements de dev et de prod. 



